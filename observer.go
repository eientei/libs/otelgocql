// Copyright The OpenTelemetry Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package otelgocql

import (
	"context"
	"time"

	"github.com/gocql/gocql"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/trace"
)

// OTelQueryObserver implements the gocql.QueryObserver interface
// to provide instrumentation to gocql queries.
type OTelQueryObserver struct {
	observer gocql.QueryObserver
	tracer   trace.Tracer
	inst     *instruments
	enabled  bool
}

// OTelBatchObserver implements the gocql.BatchObserver interface
// to provide instrumentation to gocql batch queries.
type OTelBatchObserver struct {
	observer gocql.BatchObserver
	tracer   trace.Tracer
	inst     *instruments
	enabled  bool
}

// OTelConnectObserver implements the gocql.ConnectObserver interface
// to provide instrumentation to connection attempts made by the session.
type OTelConnectObserver struct {
	ctx      context.Context
	observer gocql.ConnectObserver
	tracer   trace.Tracer
	inst     *instruments
	enabled  bool
}

// ------------------------------------------ Observer Functions

// ObserveQuery is called once per query, and provides instrumentation for it.
func (o *OTelQueryObserver) ObserveQuery(ctx context.Context, observedQuery gocql.ObservedQuery) {
	if o.enabled {
		host := observedQuery.Host
		keyspace := observedQuery.Keyspace
		inst := o.inst

		attributes := includeKeyValues(host,
			CassKeyspace(keyspace),
			CassStatement(observedQuery.Statement),
			CassRowsReturned(observedQuery.Rows),
			CassQueryAttempts(observedQuery.Metrics.Attempts),
		)

		lctx, span := o.tracer.Start(
			ctx,
			"cql.query",
			trace.WithTimestamp(observedQuery.Start),
			trace.WithAttributes(attributes...),
			trace.WithSpanKind(trace.SpanKindClient),
		)

		if observedQuery.Err != nil {
			attributes = includeKeyValues(
				host,
				CassKeyspace(keyspace),
				CassStatement(observedQuery.Statement),
			)

			span.RecordError(observedQuery.Err)
			span.SetStatus(codes.Error, observedQuery.Err.Error())
			inst.queryCount.Add(lctx, 1, metric.WithAttributes(attributes...))
		} else {
			attributes = includeKeyValues(
				host,
				CassKeyspace(keyspace),
				CassStatement(observedQuery.Statement),
			)
			inst.queryCount.Add(lctx, 1, metric.WithAttributes(attributes...))
		}

		span.End(trace.WithTimestamp(observedQuery.End))

		o := metric.WithAttributes(includeKeyValues(host, CassKeyspace(keyspace))...)
		inst.queryRows.Record(lctx, int64(observedQuery.Rows), o)
		inst.latency.Record(lctx, nanoToMilliseconds(observedQuery.Metrics.TotalLatency), o)
	}

	if o.observer != nil {
		o.observer.ObserveQuery(ctx, observedQuery)
	}
}

// ObserveBatch is called once per batch query, and provides instrumentation for it.
func (o *OTelBatchObserver) ObserveBatch(ctx context.Context, observedBatch gocql.ObservedBatch) {
	if o.enabled {
		host := observedBatch.Host
		keyspace := observedBatch.Keyspace
		inst := o.inst

		attributes := includeKeyValues(host,
			CassKeyspace(keyspace),
			CassBatchQueryOperation(),
			CassBatchQueries(len(observedBatch.Statements)),
		)

		lctx, span := o.tracer.Start(
			ctx,
			CassBatchQueryName,
			trace.WithTimestamp(observedBatch.Start),
			trace.WithAttributes(attributes...),
			trace.WithSpanKind(trace.SpanKindClient),
		)

		if observedBatch.Err != nil {
			attributes = includeKeyValues(
				host,
				CassKeyspace(keyspace),
			)

			span.RecordError(observedBatch.Err)
			span.SetStatus(codes.Error, observedBatch.Err.Error())
			inst.batchCount.Add(lctx, 1, metric.WithAttributes(attributes...))
		} else {
			attributes = includeKeyValues(host, CassKeyspace(keyspace))

			inst.batchCount.Add(lctx, 1, metric.WithAttributes(attributes...))
		}

		span.End(trace.WithTimestamp(observedBatch.End))

		o := metric.WithAttributes(includeKeyValues(host, CassKeyspace(keyspace))...)
		inst.latency.Record(lctx, nanoToMilliseconds(observedBatch.Metrics.TotalLatency), o)
	}

	if o.observer != nil {
		o.observer.ObserveBatch(ctx, observedBatch)
	}
}

// ObserveConnect is called once per connection attempt, and provides instrumentation for it.
func (o *OTelConnectObserver) ObserveConnect(observedConnect gocql.ObservedConnect) {
	if o.enabled {
		host := observedConnect.Host
		inst := o.inst

		attributes := includeKeyValues(host, CassConnectOperation())

		_, span := o.tracer.Start(
			o.ctx,
			CassConnectName,
			trace.WithTimestamp(observedConnect.Start),
			trace.WithAttributes(attributes...),
			trace.WithSpanKind(trace.SpanKindClient),
		)

		if observedConnect.Err != nil {
			attributes = includeKeyValues(host)

			span.RecordError(observedConnect.Err)
			span.SetStatus(codes.Error, observedConnect.Err.Error())
			inst.connectionCount.Add(o.ctx, 1, metric.WithAttributes(attributes...))
		} else {
			attributes = includeKeyValues(host)

			inst.connectionCount.Add(o.ctx, 1, metric.WithAttributes(attributes...))
		}

		span.End(trace.WithTimestamp(observedConnect.End))
	}

	if o.observer != nil {
		o.observer.ObserveConnect(observedConnect)
	}
}

// ------------------------------------------ Private Functions

// includeKeyValues is a convenience function for adding multiple attributes/labels to a
// span or instrument. By default, this function includes connection-level attributes,
// (as per the semantic conventions) which have been made standard for all spans and metrics
// generated by this instrumentation integration.
func includeKeyValues(host *gocql.HostInfo, values ...attribute.KeyValue) []attribute.KeyValue {
	connectionLevelAttributes := []attribute.KeyValue{
		CassDBSystem(),
		HostOrIP(host.HostnameAndPort()),
		CassPeerPort(host.Port()),
		CassVersion(host.Version().String()),
		CassHostID(host.HostID()),
		CassHostState(host.State().String()),
	}

	return append(connectionLevelAttributes, values...)
}

// nanoToMilliseconds converts nanoseconds to milliseconds.
func nanoToMilliseconds(ns int64) int64 {
	return ns / int64(time.Millisecond)
}
